$('#txt-service-bb').html('Web design encompasses many different skills and disciplines in the production and maintenance of websites. The different areas of web design include web graphic design; interface design; authoring, including standardised code and proprietary software; user experience design; and search engine optimization. Often many individuals will work in teams covering different aspects of the design process, although some designers will cover them all.[1] The term web design is normally used to describe the design process relating to the front-end (client side) design of a website including writing markup. Web design partially overlaps web engineering in the broader scope of web development. Web designers are expected to have an awareness of usability and if their role involves creating markup then they are also expected to be up to date with web accessibility guidelines.');
$('#gd-sevice-b').click(function (){
    $('#wb-sevice-b').removeClass("blue-service-block");
    $('#os-sevice-b').removeClass("blue-service-block");
    $('#ad-sevice-b').removeClass("blue-service-block");
    $('#om-sevice-b').removeClass("blue-service-block");
    $('#ss-sevice-b').removeClass("blue-service-block");
    $('#gd-sevice-b').addClass("blue-service-block");
    $('#txt-service-bb').html('Graphic design is the process of visual communication and problem-solving through the use of typography, photography and illustration. The field is considered a subset of visual communication and communication design, but sometimes the term "graphic design" is used synonymously. Graphic designers create and combine symbols, images and text to form visual representations of ideas and messages. They use typography, visual arts and page layout techniques to create visual compositions. Common uses of graphic design include corporate design (logos and branding), editorial design (magazines, newspapers and books), wayfinding or environmental design, advertising, web design, communication design, product packaging and signage.');
});
$('#wb-sevice-b').click(function(){
    $('#wb-sevice-b').addClass("blue-service-block");
    $('#os-sevice-b').removeClass("blue-service-block");
    $('#ad-sevice-b').removeClass("blue-service-block");
    $('#om-sevice-b').removeClass("blue-service-block");
    $('#ss-sevice-b').removeClass("blue-service-block");
    $('#gd-sevice-b').removeClass("blue-service-block");
    $('#txt-service-bb').html('Web design encompasses many different skills and disciplines in the production and maintenance of websites. The different areas of web design include web graphic design; interface design; authoring, including standardised code and proprietary software; user experience design; and search engine optimization. Often many individuals will work in teams covering different aspects of the design process, although some designers will cover them all.[1] The term web design is normally used to describe the design process relating to the front-end (client side) design of a website including writing markup. Web design partially overlaps web engineering in the broader scope of web development. Web designers are expected to have an awareness of usability and if their role involves creating markup then they are also expected to be up to date with web accessibility guidelines.');

});
$('#os-sevice-b').click(function(){
    $('#wb-sevice-b').removeClass("blue-service-block");
    $('#os-sevice-b').addClass("blue-service-block");
    $('#ad-sevice-b').removeClass("blue-service-block");
    $('#om-sevice-b').removeClass("blue-service-block");
    $('#ss-sevice-b').removeClass("blue-service-block");
    $('#gd-sevice-b').removeClass("blue-service-block");
    $('#txt-service-bb').html('Customer support is generally defined as service efforts from technology vendors and providers that focus on helping customers to use products and services correctly, efficiently and effectively. Many see this specific type of support as part of a larger category of customer service, but while customer support is often provided in response to customer demand, it is also part of intelligent planning for a wide variety of IT companies.');
});
$('#ad-sevice-b').click(function(){
    $('#wb-sevice-b').removeClass("blue-service-block");
    $('#os-sevice-b').removeClass("blue-service-block");
    $('#ad-sevice-b').addClass("blue-service-block");
    $('#om-sevice-b').removeClass("blue-service-block");
    $('#ss-sevice-b').removeClass("blue-service-block");
    $('#gd-sevice-b').removeClass("blue-service-block");
    $('#txt-service-bb').html('A mobile app or mobile application is a computer program or software application designed to run on a mobile device such as a phone/tablet or watch. Apps were oiginally intended for productivity assistance such as Email, calendar, and contact databases, but the public demand for apps caused rapid expansion into other areas such as mobile games, factory automation, GPS and location-based services, order-tracking, and ticket purchases, so that there are now millions of apps available. Apps are generally downloaded from application distribution platforms which are operated by the owner of the mobile operating system, such as the App Store (iOS) or Google Play Store. Some apps are free, and others have a price, with the profit being split between the application');
});
$('#om-sevice-b').click(function(){
    $('#wb-sevice-b').removeClass("blue-service-block");
    $('#os-sevice-b').removeClass("blue-service-block");
    $('#ad-sevice-b').removeClass("blue-service-block");
    $('#om-sevice-b').addClass("blue-service-block");
    $('#ss-sevice-b').removeClass("blue-service-block");
    $('#gd-sevice-b').removeClass("blue-service-block");
    $('#txt-service-bb').html('Online marketing is a set of tools and methodologies used for promoting products and services through the internet. Online marketing includes a wider range of marketing elements than traditional business marketing due to the extra channels and marketing mechanisms available on the internet.');
});
$('#ss-sevice-b').click(function(){
    $('#wb-sevice-b').removeClass("blue-service-block");
    $('#os-sevice-b').removeClass("blue-service-block");
    $('#ad-sevice-b').removeClass("blue-service-block");
    $('#om-sevice-b').removeClass("blue-service-block");
    $('#ss-sevice-b').addClass("blue-service-block");
    $('#gd-sevice-b').removeClass("blue-service-block");
    $('#txt-service-bb').html('SEO services help to ensure that a site is accessible to a search engine and improves the chances that the site will be found and ranked highly by the search engine. SEO service providers offer a wide range of services such as keyword and keyphrase optimization and research, technical website SEO audits, optimization of specific pages, robots.txt and sitemaps and additional tasks as deemed appropriate for the client');
});


